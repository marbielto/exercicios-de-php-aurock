<?php

/** 
 * Exibir lista de todos os exercicios com link, dinamicamente
 */

?>
<!DOCTYPE html>
<html>
<head>
    <title>Index</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>
<body>
    <h1>Index<h1>
    <div>
    <a href="01-iteracao.php">01-iteracao.php</a><br>
    <a href="02-tabela.php" >02-tabela.php</a><br>
    <a href="03-tabela-condicional.php" >03-tabela-condicional.php</a><br>
    <a href="04-form.php" >04-form.php</a><br>
    <a href="05-form-envio-email.php" >05-form-envio-email.php</a><br>
    <a href="06-form-validacao.php">06-form-validacao.php</a><br>
    <a href="07-leitura-csv.php" >07-leitura-csv.php</a><br>
    <a href="08-form-upload.php">08-form-upload.php</a><br>
    <a href="09-01-banco-dados.php" >09-01-banco-dados.php</a><br>
    <a href="09-02-valida-cadastro.php" >09-02-valida-cadastro.php</a><br>
    <a href="09-03-tabelas-dados.php" >09-03-tabelas-dados.php</a><br>
    <a href="10-01-login.php">10-01-login.php</a><br>
    <a href="10-02-cadastro-usuario.php">10-02-cadastro-usuario.php</a><br>
    <a href="10-03-valida-cadastro-usuario.php">10-03-valida-cadastro-usuario.php</a><br>
    <a href="10-04-tabela-usuarios.php">10-04-tabela-usuarios.php</a><br>
    <a href="11-01-sessao.php">11-01-sessao.php</a><br>
    <a href="11-02-logout.php">11-02-logout.php</a><br>
    <a href="12-index.php">12-index.php</a><br>
    </div>
    
</body>
</html>