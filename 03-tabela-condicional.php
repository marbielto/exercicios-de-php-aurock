<?php

/**
 * Exibir a lista abaixo como uma tabela html
 * Exibir a linha em verde usando <tr class="verde"> quando a coluna "diferença" for positiva (> 0)
 * Exibir a linha em vermelho usando <tr class="vermelho"> quando a coluna "diferença" for positiva (< 0)
 * Colunas: Posição, Time, Jogos, Rounds Ganhos - Rounds Perdidos, Diferença
 */


$challengers = [
    // posicao, time, vitorias, derrotas, rounds ganhos, rounds perdidos
    ['1', 'Team Liquid', '3', '0', '51', '30'],
    ['1', 'Ninjas in Pyjamas', '3', '0', '60', '43'],
    ['3', 'Astralis', '3', '1', '74', '48', '+26'],
    ['3', 'compLexity Gaming', '3', '1', '55', '54'],
    ['3', 'HellRaisers', '3', '1', '69', '70'],
    ['6', 'BIG', '2', '2', '60', '49'],
    ['6', 'North', '2', '2','71', '64'],
    ['6', 'TyLoo', '2', '2', '73', '71'],
    ['6', 'Team Spirit', '2', '2', '49', '49'],
    ['6', 'Vega Squadron', '2', '2', '67', '68'],
    ['6', 'OpTic Gaming', '2', '2', '71', '80'],
    ['12', 'Gambit Esports', '1', '3', '55', '61'],
    ['12', 'Rogue', '1', '3', '56', '67'],
    ['12', 'Renegades', '1', '3', '45', '63'],
    ['15', 'Space Soldiers', '0', '3', '36', '51'],
    ['15', 'Virtus.pro', '0', '3', '24', '48'],
];

?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <title>Challengers - Major</title>

        <style>
            table tr.verde {
                background: #2ECC40;
            }

            table tr.vermelho {
                background: #FF4136;
                color: #FFF;
            }
        </style>
        <script>

        </script>
    </head>
    <body>
        <table id="table">
            <thead>
                <tr>
                    <th>Posição</th>
                    <th>Time</th>
                    <th>Jogos</th>
                    <th>Rounds Ganhos - Rounds Perdidos</th>
                    <th>Diferença</th>
                </tr>
            </thead>
            <tbody>
            <tr class=verde>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=vermelho>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=verde>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=verde>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            <tr class=verde>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
            
            <script>
                var array = [['1º', 'HellRaisers', '4', '69 - 70', -1],
                            ['2º', 'Ninjas in Pyjamas', '3', '60 - 43', 17],
                            ['3º', 'compLexity Gaming', '4', '55 - 54', 1],
                            ['4º', 'Team Liquid', '3', '51 - 30', 21],
                            ['5º', 'Astralis', '4', '74 - 48', 26],
                            ['6º', 'TyLoo', '4', '73 - 71', 2],
                            ['7º', 'North', '4','71 - 64', 7],
                            ['8º', 'OpTic Gaming', '4', '71-80', 9],
                            ['9º', 'Vega Squadron', '4', '67-68', 1],
                            ['10º', 'BIG', '4', '60-49', 11],
                            ['11º', 'Team Spirit', '4', '49 - 49', 0],
                            ['12º', 'Gambit Esports', '4', '55 - 61', 6],
                            ['13º', 'Renegades', '4', '45 - 63', -18],
                            ['14º', 'Space Soldiers', '3', '36 - 51', -15],
                            ['15º', 'Virtus.pro', '3', '24 - 48', -24]],
                            table = document.getElementById("table");
            // rows
            for(var i = 1; i < table.rows.length; i++)
            {
            // cells
            for(var j = 0; j < table.rows[i].cells.length; j++)
            {
              table.rows[i].cells[j].innerHTML = array[i - 1][j];
            }
            }


            </script>
            </tbody>
        </table>
        <a href="12-index.php"><br>Voltar</a>
    </body>
</html>