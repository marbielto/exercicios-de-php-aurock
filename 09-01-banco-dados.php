<?php

/**
 * Desenvolver um formulario de contato (nome, email, telefone, mensagem), inserir os dados no banco e exibir todos os contatos inseridos em uma tabela.
 * ** Deve utilizar as funcoes de banco em lib/bancoContato.php **
 * ** Deve utilizar um arquivo para o formulario (esse arquivo), um arquivo para processar os dados (09-valida-cadastro.php) e um arquivo para a exibicao (09-tabela-dados.php) **
 */
include_once("conexao.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Formulario</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
    <form method="POST" action="09-02-valida-cadastro.php">
	<legend>Contato:</legend>
	<br>
	<div class="mb-3">
	<label class="form-label">Nome:</label>
	<input type="text" name="nome"  class="form-control" required placeholder="Nome completo...">
	</div>
	<div class="mb-3">
	<label class="form-label">Email:</label>
	<input type="email" name="email" class="form-control" required placeholder="exemplo@gmail.com">
	</div>
	<div class="mb-3">
    <label class="form-label">Telefone:</label>
	<br>
	<input type="number" name="telefone" class="form-control" maxlength="9" required placeholder="1999999999">
	</div>
	<div class="mb-3">
	<label class="form-label">Mensagem:</label>
    <textarea name="mensagem" class="form-control" rows=3 required maxlength="200" placeholder="Digite algo aqui..."></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
	<br>
    <a href="12-index.php"><br>Voltar</a>
</body>
</html>
