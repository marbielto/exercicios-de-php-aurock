<?php

/**
 * Exibir (utilizando echo) a matriz abaixo utilizando os três métodos: for, foreach, while e do-while
 */

$challengers = [
    'Team Liquid',
    'Ninjas in Pyjamas',
    'Astralis',
    'compLexity Gaming',
    'HellRaisers',
    'BIG',
    'North',
    'TyLoo',
    'Team Spirit',
    'Vega Squadron',
    'OpTic Gaming',
    'Gambit Esports',
    'Rogue',
    'Renegades',
    'Space Soldiers',
];

echo "<br>"."----------FOR----------"."<br>";
//imprimindo pelo comando for
for ($i=0;$i<count($challengers);++$i)
{
    echo $challengers[$i]."<br>";
}

echo "<br>"."----------FOREACH----------"."<br>";

//imprimindo por foreach
$i = 0; 
foreach ($challengers as $v) {
    echo "\$challengers[$i] => $v.\n"."<br>";
    $i++;
}
echo "<br>"."----------WHILE----------"."<br>";
// imprimindo por while
$i=0;
while($i<count($challengers))
{
    echo $challengers[$i]."<br>";
    $i++;
}

//imprimindo por do-while
echo "<br>"."----------DO-WHILE----------"."<br>";

$i=0;
do{
    echo $challengers[$i]."<br>";
    $i++;
} while($i<=14);

echo '<a href="12-index.php"><br>Voltar</a>';
