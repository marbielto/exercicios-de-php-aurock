<?php

/**
 * Desenvolver um formulario e validar os dados enviados
 * Nome completo: ter pelo menos duas palavras
 * Email: ter o formato de email
 * Telefone: apenas numerico e entre 8 e 9 digitos
 * Mensagem: maior que 5 e menor que 200 caracteres
 *
 * ** Remover os espaços extra dos valores **
 */


?>
<!DOCTYPE html>
<html>
<head>
    <title>Formulario</title>
</head>
<body>
    <form name='formulario'  action='' onsubmit='valid()'>
    <fieldset>
	<legend>Informações Pessoais:</legend>
	<br>
	<label>Nome Completo:</label>
	<br>
	<input type="text" name="nome" required minlength="8" placeholder="Digite seu nome...">
    <br>
    <label>Telefone:</label>
    <br> 
    <input pattern="^[0-9]{4,5}[0-9]{4}$" type="number" name="telefone" id="telefone" required placeholder='Digite seu número...'>
    </fieldset>
	<fieldset>
		<legend>Contato:</legend>
		<br>
		<label>Email:<label>
		<br>
		<input type="email" name="email" id="email" required placeholder="exemplo@email.com">  
		<br>
		<label>Mensagem</label>
		<br>
		<textarea name="mensagem" id="mensagem" minlength="6" maxlength="199" required placeholder="Digite aqui..."></textarea>
		<br>
        <input type='submit'>
        <input type='reset'>
        <br>
	</fieldset> 
    </form>
    <script>
        function valida(){
            if(document.getElementById('telefone').value.length<8){
                alert('Informe pelo menos 8 caracteres');
                document.getElementById('telefone').focus();
                return false;
            }
            if(document.getElementById('mensagem').value.length<5){
                alert('A mensagem deve ter pelo menos mais de 5 caracteres');
                document.getElementById('telefone').focus();
                return false;
            }
            
            if(document.getElementById('nome').value.length<8 || document.getElementById('nome').value==' '){
                alert('Informe pelo menos 2 palavras');
                document.getElementById('').focus();
                return false;
            }

        return true;
        }
    </script>
    <a href="12-index.php"><br>Voltar</a>
</body>
</html>