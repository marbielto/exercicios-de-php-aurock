<?php

/**
 * Desenvolver um formulario e exibir os valores preenchidos após o envio (escondendo o form)
 */



?>

<!DOCTYPE html>
<html>
<head>
    <title>Formulario</title>
</head>
<body>
    <form method="post" action="exibir_dados.php">
			<h1>Formulário</h1>
	<fieldset>
	<legend>Informações Pessoais:</legend>
	<br>
	Nome:
	<br>
	<input type="text" name="nome" placeholder="Primeiro nome...">
	<br>
	Sobrenome:
	<br>
	<input type="text" name="sobrenome" placeholder="Sobrenome...">
	<br>
	Gênero:
	<br>
	<input type="radio" name="genero" checked> Masculino
	<input type="radio" name="genero"> Feminino
	<input type="radio" name="genero"> Outro
	<br>
	Time:
	<br>
	<select name=time>
		<option value="palmeiras">Palmeiras</option>
		<option value="santos">Santos</option>
		<option value="corinthians">Corinthians</option>
	</select>
	</fieldset>
	<fieldset>
		<legend>Informações de Conta</legend>
		<br>
		Email:
		<br>
		<input type="email" name="email">
		<br>
		Senha:
		<br>
		<input type="password" name="senha">
		<br>
		Notificações:
		<br>
		<input type="checkbox" name="noti_email"> Email
		<input type="checkbox" name="noti_celular"> Celular
	</fieldset>
	<br>
		<input type="submit" name="enviar">
		<input type="reset" name="limpar">
	</form>
    </form>
    <br><a href="12-index.php">Voltar</a>
</body>
</html>
