<?php

// Utilizar esse arquivo para exibir os cadastros feitos no banco de dados
include_once("conexao2.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tabela Usuários</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
			<h2>Tabela Usuários</h2>
			<span id="conteudo"></span>
		</div>
		
		<script>
			$(document).ready(function () {
				$.post('listar_usuario2.php', function(retorna){
					//Subtitui o valor no seletor id="conteudo"
					$("#conteudo").html(retorna);
				});
			});
		</script>
    <br><a href="12-index.php">Voltar</a>
</body>
</html>