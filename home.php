<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Início</title>
    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
            background: linear-gradient(to right, rgb(163, 102, 255), rgb(92, 0, 230));
            text-align: center;
            color: white;
        }
        .box{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            background-color: rgb(0, 0, 0, 0.6);
            padding: 30px;
            border-radius: 10px;
        }
        a{
            text-decoration: none;
            color: white;
            border: 3px solid darkmagenta;
            border-radius: 10px;
            padding: 10px;
        }
        a:hover{
            background-color: darkmagenta;
        }
    </style>
</head>
<body>
    <a href="12-index.php">Voltar</a>
    <h1>Olá, seja Bem-vindo!</h1>
    <h2>marbielto</h2>
    <div class="box">
        <a href="10-01-login.php">Login</a>
        <a href="10-02-cadastro-usuario.php">Cadastre-se</a>
    </div>
</body>
</html>