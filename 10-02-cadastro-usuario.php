<?php

// Utilizar esse arquivo para processar os dados de cadastro do usuário


if(isset($_POST['submit']))
{
    // print_r('Email: ' . $_POST['email']);
    // print_r('<br>');

    include_once('config.php');

    $email = $_POST['email'];
    $senha = $_POST['senha'];
    //$senhaSegura = password_hash($senha, PASSWORD_DEFAULT);
    $result = mysqli_query($conexao, "INSERT INTO usuario(email, senha) 
    VALUES ('$email','$senha')");

    header('Location: 10-01-login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulário</title>
    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
            background-image: linear-gradient(to right,rgb(163, 102, 255), rgb(92, 0, 230));
        }
        a{
            text-decoration: none;
            color: white;
            border: 3px solid darkmagenta;
            border-radius: 10px;
            padding: 10px;
        }
        a:hover{
            background-color: darkmagenta;
        }
        .box{
            color: white;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            background-color: rgba(0, 0, 0, 0.6);
            padding: 15px;
            border-radius: 15px;
            width: 20%;
        }
        fieldset{
            border: 3px solid darkmagenta;
        }
        legend{
            border: 1px solid darkmagenta;
            padding: 10px;
            text-align: center;
            background-color: darkmagenta;
            border-radius: 8px;
        }
        .inputBox{
            position: relative;
        }
        .inputUser{
            background: none;
            border: none;
            border-bottom: 1px solid white;
            outline: none;
            color: white;
            font-size: 15px;
            width: 100%;
            letter-spacing: 2px;
        }
        .labelInput{
            position: absolute;
            top: 0px;
            left: 0px;
            pointer-events: none;
            transition: .5s;
        }
        .inputUser:focus ~ .labelInput,
        .inputUser:valid ~ .labelInput{
            top: -20px;
            font-size: 12px;
            color: DarkOrchid;
        }
        #submit{
            background-image: linear-gradient(to right,rgb(163, 102, 255), rgb(92, 0, 230));
            width: 100%;
            border: none;
            padding: 15px;
            color: white;
            font-size: 15px;
            cursor: pointer;
            border-radius: 10px;
        }
        #submit:hover{
            background-image: linear-gradient(to right,rgb(92, 0, 230), rgb(179, 0, 179));
        }
    </style>
</head>
<body>
    <a href="home.php">Início</a>
    <div class="box">
        <form action="10-02-cadastro-usuario.php" method="POST">
            <fieldset>
                <legend><b>Cadastro de Usuários</b></legend>
                <br>
                <br>
                <div class="inputBox">
                    <input type="email" name="email" id="email" class="inputUser" required>
                    <label for="email" class="labelInput">Email</label>
                </div>
                <br><br>
                <div class="inputBox">
                    <input type="password" name="senha" id="senha" class="inputUser" required>
                    <label for="senha" class="labelInput">Senha</label>
                </div>
                <br><br>
                <input type="submit" name="submit" id="submit" value="Enviar">
            </fieldset>
        </form>
    </div>
</body>
</html>


